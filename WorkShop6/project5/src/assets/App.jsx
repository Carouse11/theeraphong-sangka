import "./App.css";
import Navbar from "./Navbar";
import Users from "./User";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import UserCreate from "./UserCreate";
import UserUpdate from "./UserUpdate";


function App() {
  return (
    <div>
      <Navbar />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Users />} />
          <Route path="create" element={<UserCreate />} />
          <Route path="update/:id" element={<UserUpdate/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
